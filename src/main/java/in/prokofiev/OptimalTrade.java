package in.prokofiev;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * discrete-auction
 *
 * @author <a href="mailto:nprokofiev@gmail.com">Nikolay Prokofiev</a>
 *
 */
public class OptimalTrade {
    private BigInteger volume;
    private BigDecimal price;

    public OptimalTrade(BigInteger volume, BigDecimal price) {
        this.volume = volume;
        this.price = price;
    }

    public BigInteger getVolume() {
        return volume;
    }

    public void setVolume(BigInteger volume) {
        this.volume = volume;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "OptimalTrade{" +
                "volume=" + volume +
                ", price=" + price +
                '}';
    }
}
