package in.prokofiev;

import java.util.List;

/**
 * discrete-auction
 *
 * @author <a href="mailto:nprokofiev@gmail.com">Nikolay Prokofiev</a>
 *
 */
public interface Auction {
    OptimalTrade getOptimalPrice(List<Order> orders);
}
