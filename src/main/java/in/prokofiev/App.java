package in.prokofiev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 * discrete-auction
 *
 * @author <a href="mailto:nprokofiev@gmail.com">Nikolay Prokofiev</a>
 * @date 25.07.16 0:52
 */
public class App {
    public static void main(String[] args){
        ArrayList<Order> orders = new ArrayList<>();
        Auction auction = new AuctionImpl();
        try{
            BufferedReader br =
                    new BufferedReader(new InputStreamReader(System.in));

            String input;

            while((input=br.readLine())!=null){
                orders.add(parse(input));
            }

        }catch(Exception io){
            new RuntimeException(io);
        }
        OptimalTrade optimalTrade = auction.getOptimalPrice(orders);
        if(BigDecimal.ZERO.equals(optimalTrade.getVolume())){
            System.out.println("0 n/a");
            return;
        }
        System.out.println(optimalTrade.getVolume() + " " + optimalTrade.getPrice());
    }

    public static Order parse(String line) throws Exception{
        String[] slices = line.trim().split(" ");
        assert slices.length == 3;
        Order.Direction direction = "B".equalsIgnoreCase(slices[0]) ? Order.Direction.BUY : Order.Direction.SELL;
        Integer volume = Integer.parseInt(slices[1]);
        Double price = Double.parseDouble(slices[2]);
        return new Order(BigInteger.valueOf(volume), price, direction);
    }

}
