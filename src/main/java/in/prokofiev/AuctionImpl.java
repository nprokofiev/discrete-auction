package in.prokofiev;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * discrete-auction
 *
 * @author <a href="mailto:nprokofiev@gmail.com">Nikolay Prokofiev</a>
 *
 */
public class AuctionImpl implements Auction {

   public OptimalTrade getOptimalPrice(List<Order> orders){
        TreeMap<Double, Order> sell = new TreeMap<Double, Order>();
        TreeMap<Double, Order> buy = new TreeMap<Double, Order>();
        //sum volume for price dublicates
        for(Order order : orders){
            Map<Double, Order> map;
            if(Order.Direction.BUY.equals(order.getDirection()))
                map = buy;
            else
                map = sell;
            Order existedOrder = map.get(order.getPrice());
            if(existedOrder!=null){
                order = existedOrder.add(order);
            }
            map.put(order.getPrice(), order);
        }
        if(sell.size()==0 || buy.size()==0)
            return new OptimalTrade(BigInteger.ZERO, null);

       //intersection
        Order maxBuy = buy.get(buy.lastKey());
        Order lastSell = new Order(BigInteger.ZERO, 0.0, Order.Direction.SELL);

        for(Order order : sell.headMap(maxBuy.getPrice(), true).values()){
            lastSell.setPrice(order.getPrice());
            lastSell.setVolume(lastSell.getVolume().add(order.getVolume()));
        }

        Order minSell = sell.get(sell.firstKey());
        Order lastBuy = new Order(BigInteger.ZERO, 0.0, Order.Direction.BUY);
        for(Order order : buy.tailMap(minSell.getPrice(), true).descendingMap().values()){
            lastBuy.setPrice(order.getPrice());
            lastBuy.setVolume(lastBuy.getVolume().add(order.getVolume()));
        }


        int cmp = lastSell.getVolume().compareTo(lastBuy.getVolume());
        if(cmp < 0){
            return new OptimalTrade(lastSell.getVolume(), new BigDecimal(lastSell.getPrice()).setScale(2, BigDecimal.ROUND_HALF_UP));
        }
        else if(cmp > 0){
            return new OptimalTrade(lastBuy.getVolume(), new BigDecimal(lastBuy.getPrice()).setScale(2, BigDecimal.ROUND_HALF_UP));
        }
        else {
            if(lastSell.getVolume().compareTo(BigInteger.ZERO)==0)
                return new OptimalTrade(BigInteger.ZERO, null);
            BigDecimal avgPrice = new BigDecimal((lastBuy.getPrice() + lastSell.getPrice()));
            return new OptimalTrade(lastBuy.getVolume(), avgPrice.divide(BigDecimal.valueOf(2)).setScale(2, BigDecimal.ROUND_HALF_UP));
        }
    }
}
