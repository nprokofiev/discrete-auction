package in.prokofiev;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * discrete-auction
 *
 * @author <a href="mailto:nprokofiev@gmail.com">Nikolay Prokofiev</a>
 *
 */
public class Order {
    public enum Direction{
        SELL,
        BUY
    }

    private BigInteger volume;
    private Double price;
    private Direction direction;

    public Order(BigInteger volume, Double price, Direction direction) {
        this.volume = volume;
        this.price = price;
        this.direction = direction;
    }

    public BigInteger getVolume() {
        return volume;
    }

    public void setVolume(BigInteger volume) {
        this.volume = volume;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Order add(Order plus){
        assert price.equals(plus.price);
        assert direction.equals(plus.direction);
        return new Order(volume.add(plus.volume), price, direction);
    }




}
