package in.prokofiev;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * discrete-auction
 *
 * @author <a href="mailto:nprokofiev@gmail.com">Nikolay Prokofiev</a>
 *
 */
public class AuctionImplTest {

    private Auction auction = new AuctionImpl();

    @Test
    public void testGetOptimalPrice() {
          List<Order> list = new ArrayList<Order>();
          list.add(new Order(BigInteger.valueOf(100L), 15.40, Order.Direction.BUY));
          list.add(new Order(BigInteger.valueOf(100L), 15.30, Order.Direction.BUY));
          list.add(new Order(BigInteger.valueOf(150L), 15.30, Order.Direction.SELL));
        OptimalTrade optimalTrade = auction.getOptimalPrice(list);
        System.out.println(optimalTrade);
        Assert.assertEquals(0, optimalTrade.getPrice().compareTo(BigDecimal.valueOf(15.30)));
        Assert.assertEquals(0, optimalTrade.getVolume().compareTo(BigInteger.valueOf(150)));
    }

    @Test
    public void testNoIntersection(){
        List<Order> list = new ArrayList<Order>();
        list.add(new Order(BigInteger.valueOf(100L), 10.00, Order.Direction.BUY));
        list.add(new Order(BigInteger.valueOf(150L), 10.10, Order.Direction.SELL));
        OptimalTrade optimalTrade = auction.getOptimalPrice(list);
        System.out.println(optimalTrade);
        Assert.assertNull(optimalTrade.getPrice());
        Assert.assertTrue(optimalTrade.getVolume().equals(BigInteger.ZERO));
    }

    @Test
    public void testMultiplePrices(){
        List<Order> list = new ArrayList<Order>();
        list.add(new Order(BigInteger.valueOf(90), 15.00, Order.Direction.BUY));
        list.add(new Order(BigInteger.valueOf(20), 14.00, Order.Direction.BUY));
        list.add(new Order(BigInteger.valueOf(90), 16.00, Order.Direction.BUY));

        list.add(new Order(BigInteger.valueOf(80), 16.00, Order.Direction.SELL));
        list.add(new Order(BigInteger.valueOf(100), 15.00, Order.Direction.SELL));
        list.add(new Order(BigInteger.valueOf(90), 17.00, Order.Direction.SELL));

        OptimalTrade optimalTrade = auction.getOptimalPrice(list);
        System.out.println(optimalTrade);
        Assert.assertEquals(0, optimalTrade.getPrice().compareTo(BigDecimal.valueOf(15.50)));
        Assert.assertEquals(0, optimalTrade.getVolume().compareTo(BigInteger.valueOf(180)));
    }

    @Test
    public void testEmpty(){
        OptimalTrade optimalTrade = auction.getOptimalPrice(new ArrayList<>());
        Assert.assertNull(optimalTrade.getPrice());
        Assert.assertTrue(optimalTrade.getVolume().equals(BigInteger.ZERO));
    }
}