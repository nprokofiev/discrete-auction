package in.prokofiev;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;

/**
 * discrete-auction
 *
 * @author <a href="mailto:nprokofiev@gmail.com">Nikolay Prokofiev</a>
 * @date 25.07.16 1:05
 */
public class AppTest {

    @Test
    public void testParse() throws Exception {
        String line = "B 100 10.00";
        Order order = App.parse(line);
        Assert.assertEquals(Order.Direction.BUY, order.getDirection());
        Assert.assertEquals(0, order.getPrice().compareTo(10.00));
        Assert.assertEquals(0, order.getVolume().compareTo(BigInteger.valueOf(100)));
    }
}